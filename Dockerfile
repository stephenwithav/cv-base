FROM debian

RUN apt-get update && apt-get -y install build-essential clang cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev \
    libswscale-dev python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev wget

ENV CC clang
ENV CXX clang++

WORKDIR /

RUN git clone -b 2.4.13 https://github.com/opencv/opencv.git

RUN mkdir /opencv/release

WORKDIR /opencv/release

RUN cmake ..

RUN make -j `getconf _NPROCESSORS_ONLN` && make install

# Install ffmpeg
RUN wget http://johnvansickle.com/ffmpeg/releases/ffmpeg-3.1.1-64bit-static.tar.xz && \
    tar -xvf ffmpeg-3.1.1-64bit-static.tar.xz && \
    mv ffmpeg-3.1.1-64bit-static/ffmpeg /usr/local/bin/ffmpeg && \
    mv ffmpeg-3.1.1-64bit-static/ffprobe /usr/local/bin/ffprobe && \
    rm -rf ffmpeg-3.1.1-64bit-static/